<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = [
        'judul_buku',
        'pengarang',
        'slug_produk',
        'genre',
    ];

}
