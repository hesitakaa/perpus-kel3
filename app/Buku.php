<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'bukus';
    protected $fillable = [
        'Judul',
        'Pengarang',
        'Tahun',
        'Genre',
    ];

    public function user() {//user yang menginput data kategori
        return $this->belongsTo('App\User', 'user_id');
    }
}
