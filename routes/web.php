<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomepageController@index');
Route::get('/dashboard', 'DashboardController@index');
Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function() {
    // Route::resource('/daftar', 'BukuController');
    Route::resource('/produk', 'ProdukController');
    Route::resource('/pengembalian', 'PengembalianContoller');
    Route::resource('/transaksi', 'TransaksiController');
    Route::get('/cari','ProdukController@search');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
