<?php

use Illuminate\Database\Seeder;
use App\User;//model table users

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inputan['name'] = 'Hesi taka Maulana';
        $inputan['email'] = 'hesitakaa@gmail.com';//ganti pake emailmu
        $inputan['password'] = Hash::make('1234');//passwordnya 1234
        $inputan['phone'] = '081270569310';
        $inputan['role'] = 'admin';//kita akan membuat akun atau users in dengan role admin
        User::create($inputan);

    }
}
