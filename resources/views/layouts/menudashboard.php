<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="/dashboard" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview">
      <a href="/dashboard/produk" class="nav-link">
        <i class="nav-icon fas fa-folder-open"></i>
        <p>
          Daftar Buku
        </p>
      </a>
    </li>
    <li class="nav-item has-treeview">
      <a href="/dashboard/transaksi" class="nav-link">
        <i class="nav-icon fas fa-shopping-cart"></i>
        <p>
          pengembalian
        </p>
      </a>
    </li>

    <li class="nav-item">
      <a href="#" class="nav-link" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        <i class="nav-icon fas fa-sign-out-alt"></i>
        <p>
          Sign Out
        </p>
      </a>
    </li>
  </ul>
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>