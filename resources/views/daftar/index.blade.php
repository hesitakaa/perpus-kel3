@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <!-- table daftar buku -->
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Buku</h4>
          <div class="card-tools">
            <a href="{{ route('daftar.create') }}" class="btn btn-sm btn-primary">
              Baru
            </a>
          </div>
        </div>
        <div class="card-body">
          <form action="#">
            <div class="row">
              <div class="col">
                <input type="text" name="keyword" id="keyword" class="form-control" placeholder="ketik keyword disini">
              </div>
              <div class="col-auto">
                <button class="btn btn-primary">
                  Cari
                </button>
              </div>
            </div>
          </form>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            @if ($message = Session::get('error'))
              <div class="alert alert-warning">
                  <p>{{ $message }}</p>
              </div>
            @endif
            @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
            @endif
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Judul</th>
                  <th>Pengarang</th>
                  <th>Tahun</th>
                  <th>Genre</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              @foreach($itembuku as $buku))
                <tr>
                  <td>
                  {{ ++$no }}
                  </td>
                  <td>
                  {{ $buku->judul }}
                  </td>
                  <td>{{ $buku-> pengarang}}</td>
                  <td> {{ $buku-> tahun }}</td>
                  <td>{{ $buku-> genre}}</td>
                  <td>
                  <a href="{{ route('buku.edit', $buku->id) }}" class="btn btn-sm btn-primary mr-2 mb-2">
                      Edit
                    </a>
                    <form action="{{ route('buku.destroy', $buku->id) }}" method="post" style="display:inline;">
                      @csrf
                      {{ method_field('delete') }}
                      <button type="submit" class="btn btn-sm btn-danger mb-2">
                        Hapus
                      </button>
                    </form>  
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <!-- untuk menampilkan link page, tambahkan skrip di bawah ini -->
            {{ $itembuku->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection